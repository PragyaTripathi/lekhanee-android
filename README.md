# लेखनी कि-बोर्ड (Lekhanee Android) 

> **लेखनी** -- *नाम [संस्कृत]* लेख्ने साधन; कलम । 

> -- नेपाली बृहत् शब्दकोश


Lekhanee Android is a soft keyboard for Nepali languages for Android. 

This project is based on [indic-keyboard] (https://gitlab.com/smc/indic-keyboard) which in turn is based on AOSP's [LatinIME](https://android.googlesource.com/platform/packages/inputmethods/LatinIME) package.

## Screenshots

![Nepali Keyboard](https://gitlab.com/lekhanee/lekhanee-android/raw/master/graphic-assets/lekhanee/screenshot1.jpeg)

## Development

Please see the `indic-keyboard.md` file for development README.

## Issues and Improvements

Feel free to create an issue if you have suggestions for improvements, questions or a bug to report - [Issue Page](https://gitlab.com/lekhanee/lekhanee-android/issues).  


## Plans

- [x] Add Nepali transliteration keyboard layout
- [x] Add Nepali dictionary
- [ ] Add documentation on keyboard layouts and dictionary
- [ ] Add support for other Nepali languages (Maithili, Nepal Bhasa)







